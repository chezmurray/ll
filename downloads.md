version 1.1 Sunday Sept 13 2020 7pm
- fixed bug testing if config file exists
- fixed bug in sorting
- added -i flag for case insensitive sort of string fields

version 1.2 Monday Sept 14 2020 1pm
- updated help command,  ll -h


version 1.3 Monday Sepr 14 2020 1:40 pm
- corrected a regresion where commands were out
of order causing failure

version 1.4 Tuesday Sept 22 2020 9.00 pm
- updated to nushell version .20

version 1.5 Thursday Sept 24 2020 2;00 am
- removed /tmp file causing file permission errors
- internalized the .config file, no longer creates a 
   config file on user's machine. read only now.



##download https://www.mediafire.com/file/bc0va4fzlqa3hqj/ll/file


