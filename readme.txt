ll - list folders and files in nu shell format, equivalent to bash ls command.
   
   enter: ll --url -- to bring up "https://www.nushell.sh/book/en/" in default browser , Nu Reference Book
   
   FORMAT: ll [options] [folder or filename , if not specified current directory is used]
                          -- folder names with spaces must be quoted   "/my favourite folder"
                          -- recursion is handled by globbing "ls /root/**" - must be in qoutes
   -f                     -- list files only
   -a                     -- all, include hidden files .xxxxx
   -d                     -- list directories only
   -k                     -- count entries  
   -t  [column name]      -- sum given column , must be numeric
   -c '[column names]'    -- columns to display , not compatible with -x below
   -x '[column names]'    -- columns to exclude , not compatible wth -t above
   -s  [column name]      -- sort column
   -i                     -- if sorting on a character field, sort case insensitive
   -g  [column name]      -- group by column
   -u  [column name]      -- unique on specified column name, not compatible with -c above
   -p                     -- display full pathnames, default only show basename
   -l                     -- long list - same as ls -la
   -r                     -- reverse order
   -n '[nushell commands]'-- options in nu shell format , see examples below
    
   LONG
────┬────────────────────────────────────┬──────┬────────┬──────────┬───────────┬──────┬───────┬────────┬─────────┬─────────────┬─────────────
 #  │ name                               │ type │ target │ readonly │ mode      │ uid  │ group │ size   │ created │ accessed    │ modified    
────┼────────────────────────────────────┼──────┼────────┼──────────┼───────────┼──────┼───────┼────────┼─────────┼─────────────┼─────────────
  0 │ /tmp/.mount_nul9YF7nk/AppRun       │ File │        │ No       │ rwxr-xr-x │ root │ root  │  998 B │         │ 2 hours ago │ 2 hours ago 
  1 │ /tmp/.mount_nul9YF7nk/audio        │ Dir  │        │ No       │ rwxr-xr-x │ root │ root  │    0 B │         │ 2 hours ago │ 2 hours ago 
  2 │ /tmp/.mount_nul9YF7nk/bin          │ Dir  │        │ No       │ rwxr-xr-x │ root │ root  │    0 B │         │ 2 hours ago │ 2 hours ago 
  3 │ /tmp/.mount_nul9YF7nk/comments.txt │ File │        │ No       │ rw-r--r-- │ root │ root  │    0 B │         │ 2 hours ago │ 2 hours ago 
  4 │ /tmp/.mount_nul9YF7nk/lib          │ Dir  │        │ No       │ rwxr-xr-x │ root │ root  │    0 B │         │ 2 hours ago │ 2 hours ago 

   SHORT
────┬────────────────────────────────────┬──────┬────────┬─────────────
 #  │ name                               │ type │ size   │ modified    
────┼────────────────────────────────────┼──────┼────────┼─────────────
  0 │ /tmp/.mount_nullmfMbn/AppRun       │ File │  998 B │ 2 hours ago 
  1 │ /tmp/.mount_nullmfMbn/audio        │ Dir  │    0 B │ 2 hours ago 
  2 │ /tmp/.mount_nullmfMbn/bin          │ Dir  │    0 B │ 2 hours ago 
  3 │ /tmp/.mount_nullmfMbn/comments.txt │ File │    0 B │ 2 hours ago 
  4 │ /tmp/.mount_nullmfMbn/lib          │ Dir  │    0 B │ 2 hours ago 
  5 │ /tmp/.mount_nullmfMbn/lib1         │ Dir  │    0 B │ 2 hours ago 


EXAMPLES:
List all files in the current directory, display basenames (default)
  > ll -f

List all files in the current directory, display full pathanames
  > ll -f -p
  
Count the number of files in the current dikrectory
  > ll -f -k
  
Sum the size of all files in the current directory
  > ll -f -t size
 
List all files in a subdirectory
  > ll -f subdir
 
List all text files
  > ll "*.txt" - note the quotes

List long format, just files, sorted on modified date
  > ll -l -f -s modified

List long format sorted on last accessed date, name and accessed columns only, root folder
  > ll -l -s accessed -t "name accessed" /root

Find recursively all files that match a given pattern
  > ll "**/*.jpg"   - note the quotes

List Files whose size is > 1024 bytes
  > ll -f -n "where size > 1024 " /root/.config"
 
Count all files in a subdirectory
  > ll -f -k subdir
 
  
-n (Nushell format) Examples:
  > ll -n 'drop 3'
  > ll -n 'skip 3'
  > ll -n 'first 3'
  > ll -n 'last 3'  > ll 
  > ll -n 'where size >1024'
  > ll -n 'where size != 0 && size < 4kb '
  > ll -n 'shuffle'
  > ll -n 'get type'
  
List grouped by type
  > ll -g type
──────┬────────────────
 File │ [table 5 rows] 
 Dir  │ [table 7 rows] 
──────┴────────────────
   
===================================================================================
BASH ALIASES - you can create aliases for frequently used commands:

alias fcount="ll -f -k "       # count number of files in a directory
 > fcount
 > x=
 > echo 
alias fsum="ll -f -t size "    # total size of all files in a directory"
 > fsum
   

